
import UIKit

class SecondViewController: UIViewController {
    
    // MARK: - IBOutlet

    @IBOutlet weak var outletButtonPlayAgain: UIButton!
    @IBOutlet weak var myCountResaltLabel: UILabel!
    
    // MARK: - var
    
    var countResultSecond:Int = 0
    var namePlayer: String?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.swipeJumpVC()
        self.saveResult()
       
        }
    
    
    // MARK: - IBAction

        
    @IBAction func playAgainButton(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController else{return}
        
        self.navigationController?.pushViewController(controller, animated: true)
        self.clearCount()
        
    }
    
    // MARK: - func

    func saveResult(){
        let date = Date()
        let formatterString = DateFormatter()
        formatterString.dateFormat = "hh:mm dd. MM. yyyy"
        
        let string = formatterString.string(from: date)
                
        let resultSaved = (String(self.countResultSecond))
        let saveResulObject = ResultObject(result: resultSaved, date: string, namePlayer: namePlayer ?? "Player")
        
        StorageManager.shared.saveObject(object: saveResulObject )
        self.myCountResaltLabel.text = "Очки: \(String(self.countResultSecond))"
        self.outletButtonPlayAgain.dropShadow()
        
    }
    
    
    func clearCount(){
        self.countResultSecond = 0
    }
    
        
    
    @objc func backFirstViewtap(){
        
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "FirstViewController") as? FirstViewController else{return}
        
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
        
    
    func swipeJumpVC(){
        
        let swipeClear = UISwipeGestureRecognizer(target: self, action: #selector(backFirstViewtap))
        swipeClear.direction = .right
        self.view.addGestureRecognizer(swipeClear)
        
    }
    
    
    
}

extension UIButton {
    
    func dropShadow() {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 10, height: 1)
        layer.shadowRadius = self.radiusOne()
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
    }
    
    func radiusOne(radius: CGFloat = 20) -> CGFloat{
        self.layer.cornerRadius = radius
        return radius
    }
}



