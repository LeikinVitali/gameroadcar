

import Foundation
import UIKit


class StorageManager {
    
    enum Keys: String {
        case carPlayer = "carPlayer"
        case carRight = "carRight"
        case carLeft = "carLeft"
        
       case nameUser = "nameUser"
        case key = "customKey"

    }
    
    
           private let defaults = UserDefaults.standard
    
        
    
    var counter = 0
    static let shared = StorageManager()
    
    private init() {}
    
    
    func save(text: String, keys: Keys) {
        UserDefaults.standard.set(text, forKey: keys.rawValue)
    }
    
    
    
    func load(keys: Keys) -> String? {
        guard let text = UserDefaults.standard.value(forKey: keys.rawValue) as? String else { return nil }
        
        return text
    }
    
    
    
    
      func saveObject(object: ResultObject) {
          var array = StorageManager.shared.loadObject()
          array.append(object)
        defaults.set(encodable: array, forKey: Keys.key.rawValue)
      }
      
      func loadObject() -> [ResultObject] {
          let array = defaults.value([ResultObject].self, forKey: Keys.key.rawValue)
          
          if let array = array {
              return array
          } else  {
              return []
          }
      }
      
    
    
    
    
    
    
    
    
}

extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}
