

import UIKit

class FirstViewController: UIViewController {
    
    // MARK: - IBOutlet

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var startOutletButton: UIButton!
    @IBOutlet weak var settingsOutletButton: UIButton!
    @IBOutlet weak var pointsOutletButton: UIButton!
    @IBOutlet weak var startTextFieldNameUser: UITextField!
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startOutletButton.dropShadow()
        settingsOutletButton.dropShadow()
        pointsOutletButton.dropShadow()
        
    }
    
    override func viewDidLayoutSubviews() {
        self.imageView.addParalaxEffect()
        
    }
    
    
    // MARK: - IBAction
    @IBAction func myStartButton(_ sender: UIButton) {
        
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController else{return}
        
        controller.namePlayer = startTextFieldNameUser.text
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    
    @IBAction func mySettingsButton(_ sender: UIButton) {
        guard let controllerSettings = self.storyboard?.instantiateViewController(withIdentifier: "SetingViewController") as? SetingViewController else{return}
        self.navigationController?.pushViewController(controllerSettings, animated: true)
    }
    
    
    
    @IBAction func myPointsButton(_ sender: UIButton) {
        
        guard let controllerSettings = self.storyboard?.instantiateViewController(withIdentifier: "ResultViewController") as? ResultViewController else{return}
        self.navigationController?.pushViewController(controllerSettings, animated: true)
                
    }
    
}




extension UIView {
    func addParalaxEffect(amount: Int = 20) {
        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontal.minimumRelativeValue = -amount
        horizontal.maximumRelativeValue = amount
        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        vertical.minimumRelativeValue = -amount
        vertical.maximumRelativeValue = amount
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        addMotionEffect(group)
    }
}



