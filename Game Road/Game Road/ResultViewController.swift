

import UIKit

class ResultViewController: UIViewController {
    
    // MARK: - var

    var arrayResult:[ResultObject] = []
    
    // MARK: - viewDidLoad

    override func viewDidLoad() {
        super.viewDidLoad()
        swipeReturnVC()
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.arrayResult = StorageManager.shared.loadObject()
        self.arrayResult.reverse()
          
    }
    
    // MARK: - func

        
    @objc func backFirstViewtap(){
        
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "FirstViewController") as? FirstViewController else{return}
        
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    
    
    func swipeReturnVC(){
        
        let swipeClear = UISwipeGestureRecognizer(target: self, action: #selector(backFirstViewtap))
        swipeClear.direction = .right
        self.view.addGestureRecognizer(swipeClear)
        
    }
    
}


extension UILabel {
    
    func displayLabel(){
        
        self.layer.cornerRadius = 500
        self.alpha = 0.5
        self.backgroundColor = .black
        self.textColor = .systemBlue
    }
    
}

extension ResultViewController: UITableViewDelegate, UITableViewDataSource {
    
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return  arrayResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as? CustomTableViewCell else{
            return UITableViewCell()
        }
        
        
    
        cell.configure(object: arrayResult[indexPath.row])
       
        return cell
    }
    
   
    
}
