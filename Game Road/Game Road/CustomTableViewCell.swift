
import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var namePlayerLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!

    
    func configure(object: ResultObject){
        self.namePlayerLabel.text = object.namePlayer
        self.dateLabel.text = object.date
        self.pointsLabel.text = object.result
        
    }
    
    
}
