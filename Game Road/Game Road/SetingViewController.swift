
import UIKit

class SetingViewController: UIViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet  var carPlayerImage: UIImageView!
    @IBOutlet weak var carDangerRightImage: UIImageView!
    @IBOutlet weak var carDangerLeftImage: UIImageView!
    
    // MARK: - var
    
    var count = 0
    var arrayImage = ["car", "carTwo", "carThree"]
    var carPlayer: String = "car"
    var carRight: String = "carTwo"
    var carLeft: String = "carThree"
    
    // MARK: - viewDidLoad

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.swipeReturnVC()
        
        self.carPlayerImage.image = UIImage(named: StorageManager.shared.load(keys: .carPlayer) ?? "car")
        self.carDangerRightImage.image = UIImage(named: StorageManager.shared.load(keys: .carRight)  ?? "carThree")
        self.carDangerLeftImage.image = UIImage(named: StorageManager.shared.load(keys: .carLeft) ?? "carTwo")
        
    }
    
    
    // MARK: - IBAction
    
    @IBAction func carPlayerNextButton(_ sender: UIButton) {
        carPlayer = stepNextImage(carImage: carPlayerImage)
        StorageManager.shared.save(text: carPlayer, keys: .carPlayer)
        
    }
    
    
    
    @IBAction func carDangerRightButton(_ sender: UIButton) {
        carRight = stepNextImage(carImage: carDangerRightImage)
        StorageManager.shared.save(text: carRight, keys: .carRight)
        
        
    }
    
    
    @IBAction func carDangerLeftButton(_ sender: UIButton) {
        carLeft = stepNextImage(carImage: carDangerLeftImage)
        StorageManager.shared.save(text: carLeft, keys: .carLeft)
        
    }
    
        
    
    @IBAction func returnVCButton(_ sender: UIButton) {
        returnVC()
        
    }
    
    // MARK: - func

    
    func stepNextImage(carImage: UIImageView) -> String {
        
        count += 1
        if count > (arrayImage.count - 1){
            count = 0
        }
        
        let imageV = UIImage(named: arrayImage[count])
        let imageCar = UIImageView(image: imageV)
        imageCar.frame = CGRect(x:carImage.frame.origin.x, y: carImage.frame.origin.y, width: CGFloat(Int( carImage.bounds.width)), height: CGFloat(Int(carImage.bounds.height)))
        view.addSubview(imageCar)
        
        UIImageView.animate(withDuration: 0.3) {
            
            carImage.image = imageCar.image
            imageCar.image = nil
        }
        
        
        return arrayImage[count]
        
    }
    
        
    
    @objc func backFirstViewtap(){
        
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "FirstViewController") as? FirstViewController else{return}
        
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    
    func swipeReturnVC(){
        
        let swipeClear = UISwipeGestureRecognizer(target: self, action: #selector(backFirstViewtap))
        swipeClear.direction = .right
        self.view.addGestureRecognizer(swipeClear)
        
    }
    
    
    
    func returnVC(){
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    
    
}







