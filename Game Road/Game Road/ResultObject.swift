
import Foundation


class ResultObject: Codable {
    var result: String?
    var date: String?
    var namePlayer: String?
    
    init(result: String, date: String, namePlayer: String ) {
        self.result = result
        self.date = date
        self.namePlayer = namePlayer
    }
    
    public enum CodingKeys: String, CodingKey {
        case result, date, namePlayer
    }

    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.date = try container.decodeIfPresent(String.self, forKey: .date)
        self.result = try container.decodeIfPresent(String.self, forKey: .result) ?? String()
        self.namePlayer = try container.decodeIfPresent(String.self, forKey: .namePlayer) ?? String()

    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(self.result, forKey: .result)
        try container.encode(self.date, forKey: .date)
        try container.encode(self.namePlayer, forKey: .namePlayer)

    }

}
